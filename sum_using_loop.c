#include <stdio.h>

int main()
{
    int n,r,sum;
	printf("enter a number\n");
    scanf("%d",&n);
    for(sum=0; n!=0; n/=10)
    {
        r = n%10;
        sum+=r;
    }
    printf("sum of all digits of the given number is %d\n",sum);
	return 0;
}