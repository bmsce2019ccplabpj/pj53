#include <stdio.h>

int main()
{ 
    int i,j,marks[5][3],high;
    printf("Enter the marks in 3 courses of 5 students (on 100)\n");
    for(i=0; i<5; i++)
    {
        for(j=0; j<3; j++)
        {
           scanf("%d",&marks[i][j]);    
        }
    }
    for(j=0; j<3; j++)
    {
       high = marks[0][j];
       for(i=1; i<5; i++)
       {
           high = (high>marks[i][j])?high:marks[i][j];
       }
       printf("Highest marks in course %d = %d\n",j+1,high);    
    }
	return 0;
}