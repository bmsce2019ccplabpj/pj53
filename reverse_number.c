#include <stdio.h>

int main()
{
    int n,r,rev;
	printf("Enter a number\n");
    scanf("%d",&n);
    for(rev = 0; n!=0; n/=10)
    {
        r = n%10;
        rev = rev*10 + r;
    }
    printf("reverse of the given number is %d\n",rev);
	return 0;
}