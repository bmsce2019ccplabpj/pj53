#include <stdio.h>
int main()
{
    char a;
    FILE *p;
    p=fopen("INPUT.txt","w");
    printf("Enter the data to INPUT.txt file\n");
    while((a=getchar())!=EOF)
    {
        fputc(a,p);
    }
    fclose(p);
    printf("The file contents are:\n");
    p=fopen("INPUT.txt","r");
    while((a=getc(p))!=EOF)
    {
        putchar(a);
    }
    fclose(p);
    return 0;
}