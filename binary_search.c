#include <stdio.h>
int main()
{
    int i,beg,end,mid,s,n,p;
    printf("Enter number of elements in the array: ");
    scanf("%d",&n);
    int elem[n];
    printf("\nEnter %d elements of the array in ascending or:\n",n);
    for(i=0; i<n; i++)
    {
       scanf("%d",&elem[i]);
    }
    beg=0;
    end=n-1;
    printf("Enter the search element: ");
    scanf("%d",&s);
    i=0;
    while(beg<=end)
    {
       mid = (beg+end)/2;
       if(elem[mid]==s)
       {
          p=mid;
          i=1;
          break;
       }
       else if(elem[mid>s])
          beg = mid+1;
       else
          end = mid-1;
    }
    if(i=1)
      printf("Position of the search element: %d\n",p);
    else
    {
      printf("Element not found\n");
    }
    return 0;
}